<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PositionRepository
 */
interface PositionRepository extends RepositoryInterface
{
    
}
